const JEST_CONTROL_FUNCTIONS = [
  "describe",
  "it",
  "beforeEach",
  "afterEach",
  "beforeAll",
  "afterAll"
];

const isArgumentToCall = (path, functionName) => {
  const callExp = path.parent;

  if (callExp && callExp.type !== "CallExpression") {
    return false;
  }

  const { callee } = callExp;

  return callee && callee.name === functionName;
};

const isArgumentToJestCall = path =>
  JEST_CONTROL_FUNCTIONS.map(x => isArgumentToCall(path, x));

const createArrowFunctionExpression = j => path =>
  j.arrowFunctionExpression(path.node.params, path.node.body, path.node.async);

module.exports = (fileInfo, j, options, root) => {
  root
    .find(j.FunctionExpression)
    .filter(isArgumentToJestCall)
    .replaceWith(createArrowFunctionExpression(j));

  return root;
};

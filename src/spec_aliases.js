const mapToJestAliases = (path) =>
  path.replace(/^spec\/helpers/, "helpers").replace(/^spec/, "jest");

module.exports = (fileInfo, j, options, root) => {
  root.find(j.ImportDeclaration).forEach((x) => {
    x.value.source.value = mapToJestAliases(x.value.source.value);
  });

  return root;
};

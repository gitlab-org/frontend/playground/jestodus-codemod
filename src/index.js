const transformers = [
  require("./arrow_functions"),
  require("./set_timeout_to_set_immediate"),
  require("./spec_aliases"),
];
const postSourceTransformers = [
  // Leaving this commented out by default because it can throw errors by default...
  // This specific transform can be run individually with something like:
  // ```
  // jscodeshift -t ../../jestodus-codemod/node_modules/jest-codemods/dist/transformers/jasmine-this spec/javascripts/bogu.js
  // ```
  //
  // require("jest-codemods/dist/transformers/jasmine-this").default,
  require("jest-codemods/dist/transformers/jasmine-globals").default,
];

module.exports = (fileInfo, api, options) => {
  const j = api.jscodeshift;
  const root = j(fileInfo.source);

  const source = transformers
    .reduce((x, transform) => transform(fileInfo, j, options, x), root)
    .toSource();

  return postSourceTransformers.reduce(
    (x, transform) => transform({ ...fileInfo, source: x }, api, options),
    source
  );
};

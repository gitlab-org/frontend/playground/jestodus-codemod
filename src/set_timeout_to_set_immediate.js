const isEmptySetTimeout = path => {
  const funcName = path.node.callee.name;
  const tick =
    path.node.arguments.length < 2 ? 0 : path.node.arguments[1].value;

  return funcName === "setTimeout" && !tick;
};

module.exports = (fileInfo, j, options, root) => {
  const createSetImmediateCall = path =>
    j.callExpression(j.identifier("setImmediate"), [path.node.arguments[0]]);

  root
    .find(j.CallExpression)
    .filter(isEmptySetTimeout)
    .replaceWith(createSetImmediateCall);

  return root;
};

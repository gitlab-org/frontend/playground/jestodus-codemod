## Jestodus codemod

A codemod to help assist [the Jestodus](https://gitlab.com/groups/gitlab-org/-/epics/895).

#### How do I use this thing?

**One time setup:**

1. Install `jscodeshift`

```
yarn global add jscodeshift
```

2. Check out this repository somewhere
3. Navigate to where the repository was cloned
4. Install the project's dependencies

```
yarn
```

**To use:**

1. In the GitLab project, move over some Karma files

```
cp -r spec/javascripts/foo/* spec/frontend/foo
rm -r spec/javascripts/foo
```

2. Run `jscodeshift` on the new files

```
cd ../../path/to/jestodus-codemod
jscodeshift --parser=babylon -t src/index.js ../gdk/gitlab/spec/frontend/foo
```

3. Run `jest`

```
yarn run jest spec/frontend/foo
```

4. Manually tweak until Jest tests succeed :)

#### Why do I need to specify `--parser=babylon`?

It looks like `jscodeshift` has a hard time finding our babel config...

Since `jscodeshift` uses the `babel` parser by default (but can't find our config), there is a high chance
that you will run into very strange errors like this 👇 if you don't specify `--parser=babylon`.

```
Transformation error (Cannot read property 'map' of undefined)
TypeError: Cannot read property 'map' of undefined
    at Lines.join (/node_modules/recast/lib/lines.js:514:18)
    at printFunctionParams (/node_modules/recast/lib/printer.js:2093:43)
    at printMethod (/node_modules/recast/lib/printer.js:2036:61)
```

#### What's a codemod?

It's a "transform" module used by Facebook's [very cool jscodeshift](https://github.com/facebook/jscodeshift).

Basically a function that takes in file source (and other information) and returns source.

```
Source => Source
```

You can use any library to help migrate the source, but `jscodeshift` also comes baked in with it's own API, which is
rather nice.

#### Why do I need to run the codemod from the jestodus-codemod directory?

When you run the codemod, it attempts to resolve `core-js` from the current directory's Node modules
which might fail if you're in another directory. To make sure that the codemod resolves its
dependencies properly always run it from its own directory.

#### I really want to contribute!

I suggest checking out some of the existing transformers in this project. Also, use [`astexplorer`](https://astexplorer.net/)
to help you.

#### Prior art

- The original Jestodus effort [`migrate-karma-to-jest`](https://gitlab.com/gitlab-org/frontend/playground/migrate-karma-to-jest)
- https://github.com/skovhus/jest-codemods
